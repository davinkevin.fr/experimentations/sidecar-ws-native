FROM gradle:8.3 as java-build

WORKDIR /project

COPY build.gradle.kts settings.gradle.kts /project/
COPY src /project/src/

RUN gradle jar
RUN cp build/libs/sidecar-ws-native-1.0-SNAPSHOT.jar /opt/simple-sidecar.jar

FROM ghcr.io/graalvm/native-image-community:21 as native-build

WORKDIR /opt
COPY --from=java-build /opt/simple-sidecar.jar /opt/simple-sidecar.jar

RUN native-image -jar /opt/simple-sidecar.jar --static

FROM gcr.io/distroless/base-nossl:nonroot

COPY --from=native-build /opt/simple-sidecar /opt/simple-sidecar

ENV APP_PORT 6666

ENTRYPOINT ["/opt/simple-sidecar"]
