plugins {
    kotlin("jvm") version "1.8.0"
    application
}

group = "io.gitlab.davinkevin.sidecar.ws"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    jvmToolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

application {
    mainClass.set("io.gitlab.davinkevin.sidecar.ws.MainKt")
}

tasks.jar {
    manifest { attributes["Main-Class"] = "io.gitlab.davinkevin.sidecar.ws.MainKt" }
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
