package io.gitlab.davinkevin.sidecar.ws

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import java.net.InetSocketAddress
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.Executors
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.io.path.isRegularFile
import kotlin.io.path.name

fun main() {
    val port = Result.runCatching { System.getenv("APP_PORT").toInt() }.getOrElse { 7777 }

    HttpServer
        .create(InetSocketAddress("0.0.0.0", port), 0).apply {
            executor = Executors.newFixedThreadPool(10)
            createContext("/download").apply {
                setHandler(::serveFirstFileAvailable)
            }
        }
        .start()
        .also { println("Server started on port $port") }
}

fun serveFirstFileAvailable(exchange: HttpExchange) {
    val folder = Path.of(System.getenv("APP_FOLDER") ?: "/opt")

    exchange.sendResponseHeaders(200, 0)
    val oz = exchange.responseBody.buffered().let(::ZipOutputStream)

    Files.walk(folder).filter { it.isRegularFile() }.forEach {
        println("add entry: ${it.name}")
        oz.putNextEntry(ZipEntry(it.name))
        Files.copy(it, oz)
        oz.closeEntry()
    }
    oz.close()
}
